var frame_counter = 0;
var FPS = 1;

if(window.location.href.indexOf('herokuapp') > -1){ // if running on heroku
	var nlib_host ='nlib-server.herokuapp.com';
	var nlib_port =80;

}else{
	var nlib_host ='localhost';
	var nlib_port = 9000;
}

// declare wars for DOM objects:
var frame_div, state_div, input_div, random_div, peer_name_div, peer_labeling;

function main(){
	frame_div = $('#frame_count');
	peer_name_div = $('#my_peer_id');
	state_div = $('#state');
	input_div = $('#input');
	random_div = $('#random');

	connected_div = $('#connected');
	waiting_div = $('#waiting');
	peer_labeling = $('#peer_labeling');

	nlib.init(game_update, {log_level: 0, server_info: {host: nlib_host, port: nlib_port, path:'/peerjs_server'}});

}

var game_update = function(state, user_input){
	state.test = state.test || [0];
	state.test[0] = nlib.random();
	
	// pre-end

	var info = nlib.get_info();
	peer_name_div.text("my peer id: " + nlib.get_info().my_peer.id);
	frame_div.text("frame number: " + frame_counter);
	connected_div.text("connected peers: " + JSON.stringify(info.peers_connected));
	waiting_div.text("peers waiting for connection: " +JSON.stringify(info.candidate_connections));
	peer_labeling.text("peer order is: " + JSON.stringify(info.peer_order));
	state_div.text("state: " + JSON.stringify(state));
	input_div.text("user input: " + JSON.stringify(user_input));

	random_div.text("random numbers: " + JSON.stringify(info.random_numbers));

	// end
	frame_counter++;
	nlib.submit_frame(state, []);
};

// DEPRECATED
var post_init = function(){

	createjs.Ticker.framerate = FPS;
	createjs.Ticker.addEventListener("tick", handleTick);

}
