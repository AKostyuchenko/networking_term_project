//ToDo: Need to find out how to access the list of connected peers and adjust the update()
//		function to match it.

function KeyboardInputManager() {

  //var intervalID = setInterval(this.update, 32);
  keyboard = this; //global reference to KeyboardInputManager Object.
  this.events = {};
  this.inputQueue = []; //contains these arrays: [event, data]
  this.listen();		//if the event has no associated data, index 1 is undefined.
}

KeyboardInputManager.prototype.on = function (event, callback) {
  if (!this.events[event]) {
    this.events[event] = [];
  }
  this.events[event].push(callback);
};

KeyboardInputManager.prototype.emit = function (event, data) {
  var callbacks = this.events[event];
  if (callbacks) {
    callbacks.forEach(function (callback) {
      callback(data);
    });
  }
};

/*	The update function is what we need to change to make 2048 network-ready.
	I'll write in some pseudo code to help get things started.
*/

KeyboardInputManager.prototype.update = function(state, input){
	
	/*console.log(GM.init_flag);
	console.log(state);
	console.log(input);
	
	console.log(nlib.get_info());*/
	
	if(GM.init_flag){
		GM.init_flag = false;
		
		//get the game started.
		GM.addStartTiles();
		GM.actuate();
	}

	
	
	else{
		console.log("running the real game!");
		//This sets the current game state to match the one given by the library.
		//GM is a global variable pointing at the game_manager object.
		GM.grid		   = new Grid(state.grid.size,
								  state.grid.cells);
		GM.score       = state.score;
		GM.over        = state.over;
		GM.won         = state.won;
		GM.keepPlaying = state.keepPlaying;

		//This block of code relies on having access to a list of peers. I'm not sure how the 
		//library will implement this, so for now I'm using peerList as placeholder.
		var peer_list = nlib.get_peers();
		
		for(var peerIndex = 0; peerIndex < peer_list.length; peerIndex++){
			//input[peerList[peerIndex]] is one particular player's input queue.
			//writing it out every time is kinda messy, but it saves us from unnecessarily 
			//copying what might be a sizeable array of inputs.
			
			//The input queue is an array of arrays of this form: [[event, data],[event, data]]
			
			//This loop applies a single player's inputs to the game state.
			if(input[peer_list[peerIndex]]){
				//check built in for undefined input, caused by packet loss.
				for(i=0;i<input[peer_list[peerIndex]].length;i++){
					var event = input[peer_list[peerIndex]][i][0];
					
					//Some events don't have data associated with them.
					if( input[peer_list[peerIndex]][i][1] != undefined ){
						var data = input[peer_list[peerIndex]][i][1];
					}
					
					//call event
					keyboard.emit(event, data);
				}
			}
		}
	}
	
	nlib.submit_frame(GM.serialize(), keyboard.inputQueue);
	
	keyboard.inputQueue=[]; //clears the inputQueue
};


KeyboardInputManager.prototype.listen = function () {
  var self = this;

  var map = {
    38: 0, // Up
    39: 1, // Right
    40: 2, // Down
    37: 3, // Left
    75: 0, // Vim up
    76: 1, // Vim right
    74: 2, // Vim down
    72: 3, // Vim left
    87: 0, // W
    68: 1, // D
    83: 2, // S
    65: 3  // A
  };


	//}
  // Respond to direction keys
  document.addEventListener("keydown", function (event) {
    var modifiers = event.altKey || event.ctrlKey || event.metaKey ||
                    event.shiftKey;
    var mapped    = map[event.which];

    if (!modifiers) {
      if (mapped !== undefined) {
        event.preventDefault();
		
		//Push event call and data to inputQueue instead of emitting normally.
		keyboard.inputQueue.push(["move", mapped]);
        //self.emit("move", mapped);
      }
    }
		


    // R key restarts the game
    if (!modifiers && event.which === 82) {
      self.restart.call(self, event);
    }
  });

  // Respond to button presses
  this.bindButtonPress(".retry-button", this.restart);
  this.bindButtonPress(".restart-button", this.restart);
  this.bindButtonPress(".keep-playing-button", this.keepPlaying);

  
};

KeyboardInputManager.prototype.restart = function (event) {
  event.preventDefault();
  //this.emit("restart");
  if(GM.waiting_to_start == true){
	GM.waiting_to_start = false;
	nlib.start_game();
	}
  else{
	this.inputQueue.push(["restart"]);
  }
};

KeyboardInputManager.prototype.keepPlaying = function (event) {
  event.preventDefault();
  this.inputQueue.push(["keepPlaying"]);
};

KeyboardInputManager.prototype.bindButtonPress = function (selector, fn) {
  var button = document.querySelector(selector);
  button.addEventListener("click", fn.bind(this));
  button.addEventListener(this.eventTouchend, fn.bind(this));
};
