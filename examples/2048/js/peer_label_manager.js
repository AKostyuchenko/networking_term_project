function peerLabelManager(){
	this.initialization = function(){
		this.peerID = 0;
		this.peer_list = [0];
		this.peer_count = 1;
		this.next_ID = 1;
		this.joinQueue = [];
	};
	
	/**
	Receive an initial join request
	send the information to other peers in the network using sendJoinNotification
	*/
	this.receiveJoinRequest = function(peerIP){
		this.joinQueue[length(joinQueue)] = {"IP":peerIP,"notificationCount":1};
		packet = sendJoinNotification();
		
	};
	
	/**
	Create a packet to send notifications to other peers in the network
	*/
	this.sendJoinNotification = function(){
		packet = {};
		packet.size = length(this.joinQueue);
		packet.queue = joinQueue;
		return packet;
	};
	
	/**
	Receives the join notifications from other peers in the network
	*/
	this.receiveJoinNotification = function(packet){
		packet.notificationCount += 1;
		return packet;
	};
}


