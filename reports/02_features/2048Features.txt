I looked for the list of planned features and functions, but failed to find it. 
Sorry if some of this ends up being redundant.

Anyway, here are the features that are necessary to make 2048 function as a multiplayer game:

create_state: 
	Specify the data that makes up the game state.
	Returns the state object.

submit_state/submit_changes: 
	We need a way to send data to the peers.
	Returns nothing?
	
all_recieved:
	Some sort of check that waits until it has received a state from each peer.
	Necessary for lockstep implementation, and is probably useful for full-state merging in any implementation.
	May also have a check in here for dropping peers, and ways to handle that gracefully.
	Returns nothing?
	
is_turn:
	Check that determines whether it's this peer's duty to handle object generation/random number generation.
	Returns True or False.
	
merge_state:
	Compare data between received states, and make decisions about the final "real" state.
	
	***IMPORTANT***
	This must be deterministic. If not, it undermines the whole point of synchronizing game states, and could
	lead to tons of problems down the line.
	
	Different data will need to be merged in different ways.
		
		Some data should be replaced. One peer might be authoritative on particular data, and cannot be 
		argued with. Input could be a special case of this. Players cannot determine others' inputs, and 
		each is authoritative over their own data. Maybe allow developers to determine "default" inputs 
		for other players	if we end up implementing rollback.
		
		Some data should be averaged. In a non-deterministic game, there may be slight floating point 
		differences between peers' game states. Eventually this can lead to major desyncs. 
		Averaging data (or taking turns deciding whose is valid) will prevent that from happening.
		
		Some data should be voted on? If there are multiple versions of data that *should* be deterministic
		we could have the peers "vote" on the most correct result. 
			Potential problem:
			If some data depends on other data, we could have cascading problems that are more difficult to 
			solve. For example: Player's position (a non-deterministic, to-be-averaged piece of data) 
			determines whether a spike-ball trap activates (a 1-or-zero piece of data) which then moved a tiny
			amount (non-deterministic again). If the player is determined to have a position that does not
			trigger the trap, will the merged game state properly reflect the cascading effects of that 
			decision?
			
			I'll think on it some more.
		
		Might need other options as well.
		
	Returns the merged state.
		
get_authoritative_state:
	This will be used by new peers connecting to a game. It might also be used by peers who need to receive
	random numbers/generated objects from the peer whose turn it is. If instead we decide to get random numbers
	from the peer, we can...
	
get_random_numbers:
	This only handles the problems of random number generation in a lockstep implementation (as does the 
	get_authoritative state) but this could be an optimization for when users only need random numbers.
	The "active" peer generates random numbers as needed to finish processing the frame, putting the random
	numbers inside a data structure as it goes. When the is_turn actions finish it sends those random 
	numbers to all other peers so they can use the numbers to make their own calculations. 
	
	Might not be entirely perfect, though, since non-deterministic factors might lead to a user not properly
	consuming all random numbers. It would be smoothed out during the next merge_state, but might cause some
	temporary confusion.
	
May also need some way to get a list of the peers in the game, but that might be handled on the back end.

		
		