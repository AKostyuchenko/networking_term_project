// notice global variable:
nlib =
(function nlib(){
// BEGIN closure
/*--------------------------------------------------------------------------------*/
// PRIVATE
var Peer = require('peerjs');
var stable_stringify = require('json-stable-stringify');
var jquery = $ = require('jquery');
require('string_score'); // adds .score function to String's prototype

//---
var frame_submissions = {}; // frame submissions from all peers
// how many submissions were made (to determine whether all people submitted:
var submission_count = 0; // DEPRECATED?
var submission_check = {}; // names of peers that made submission for this frame

// "future" versions of above variables.
// used to store data intended for a later frame,
// applied in reset_variables() after the peer finishes dealing with packet loss.
var future_frame_submissions = {};
var future_submission_count = 0;
var future_submission_check = {};

// Flag is set if peer believes it is experiencing packet loss.
// Stops accepting current frames from peers, focuses on just finishing state
// and quickly calculating the next one.
var fixing_packet_loss = false; 

// switch that lets the all_data_received function to know if it should
// finalize frame submission (if first stage was already done)
// or just to set the ready flag for finalization
var first_stage_of_frame_submission_done = false;
var ready_to_finalize_frame_submission = false;

// random numbers
var random_numbers = [];
var nth_num = 0; // current index for returning num

// peer name to the corresponding connection
var connections = {};
// connections that must be made, but were not established yet, used to make sure that connection
// is operational before trying to use it. Set during initialization stage.
var connections_in_progress = {};

// is current peer the one to play central part in labeling/connection negotiation?
var my_peer = null; // my peer object, created during init stage
var initialized = false;
var initialization_in_progress = false;
//This is used for the intermediate step between finishing initialization and starting the game.
var initialization_ready_to_complete = false; 

//variables from from connect() and on_connection_successfully_established().
//Ensures that initialization is actually complete.
var all_connected = false;
var received_all_connections = false;

// set during find_peers_and_connect, tracks connections left to establish for initialize stage
var initial_connections_check = {started: false}; 

var options = {}; // options set for the library
var WAIT_BEFORE_CONNECTING = 15000; // in ms; how long to wait before connection to server.


// primitive logging mechanism
// TODO: generalize for all browsers
var LOG_LEVEL = 1; // 0 - info; 1 - warning; 2 - error;
var log = function(){
	if(LOG_LEVEL <= 0){ // only when log lvl is info (or lower, but nothing should be lower)
	console.log.apply(console, arguments);}}; // for development
var war = function(){
	if(LOG_LEVEL <= 1){ // if log lvl is  info or warning
	console.warn.apply(console, arguments);}};
var err = function(){
	if(LOG_LEVEL <= 2){ // if log lvl is  info or warning or error (check in case want to add fatal)
	console.error.apply(console, arguments);}};

// developer supplied function that will be called for each frame processing
var game_update_function;

// connected peer ids in some order to signify their priority for various processing,
// this array is populated during last stages of the initialization:
var peer_order = [];

// since this library is responsible for calling user's update function,
// it can set FPS
var max_fps = 30;
var min_frame_length_ms = 1000/max_fps;
var timestamp_begin = new Date().getTime();
//variable included in frame object to discover packet loss.
var current_frame = 0;

/*
 * Data frame object initializer
 */
var DataFrame = function(state, user_input, random_submission){
	var frame = {state: state, user_input: user_input, random_submission: random_submission, 
					frame: current_frame};
	return frame;
};


/*	
 *  Called on connection with peer server (after peer is created)
 *	id -- id of my peer that was just created
 */ 
var init_second_stage = function(id){
	log("Successfully created peer w/ id: ", id);

	my_peer.on("close", function(){on_peer_destroyed(id);});
	my_peer.on("connection", on_new_connection_to_this_peer);
	my_peer.on("disconnected", function(){on_peer_server_disconnection(my_peer)});

	// give server time to update before fetching the connected peers data
	setTimeout(function(){find_peers_and_connect(id);}, WAIT_BEFORE_CONNECTING);

};

/*	
 *	Called after the timeout for server update has passed
 *	Retrieves data from the server, and calls connect function
 */
var find_peers_and_connect = function(id){
	$.ajax({
			success: function(data){
				log("Currently connected to server are:", data);

				// setup peer order array:
				peer_order = data.sort();

				var to_connect_array = [];
				// check for peers who are not yet connected
				for(var i = 0; i < data.length; i++){
					var candidate = data[i];
					if(candidate != my_peer.id){
						if(!connections[candidate] && !connections_in_progress[candidate]){
							// if not yet trying to connect to this peer, add to connect queue
							to_connect_array.push(candidate);
						}

						// need to make sure to connect to every known peer before finishing
						// initialization, hence this object
						initial_connections_check[candidate] = true; 
					}

				}

				for(var key in initial_connections_check){
					// this loop is done so if some connection successfully initialized
					// when array was in the process of population, keys are still
					// successfully removed
					if(connections[key]){
						delete initial_connections_check[key];
					}
				}
				
				initial_connections_check.started = true;

				connect(to_connect_array, my_peer.id);
			},

			type: 'GET',
			dataType: 'json',
			url: "/api/v1",
			error: function(err){
				log("Error: ajax request for peer array failed", err);
			},
			port: options.server_info.port

	});

};

/* 
 *	This function handles the connection from my peer to the specified peers
 * 	input:
 * 	peer_ids(array) -- list of all peer ids that participate in the game
 * 	my_id -- one of ids from array that corresponds to this peer's id
 */
var connect = function connect(peer_ids, my_id){
	// TODO: set label, metadata, serialization, reliable
	
	//var my_id = my_peer.id;
	
	log("Preparing to connect to:", peer_ids, "My id: ", my_id);

	for(var i = 0; i < peer_ids.length; i++){
		var id = peer_ids[i];
		if(id != my_id){
			var con = my_peer.connect(id);
			connections_in_progress[id] = true; // connection is being established
			con.on("open", on_connection_successfully_established);
						
		}
	}
	
	if(peer_ids.length == 0){
		received_all_connections = true;
	
	}
	
	all_connected = true;
	init_last_stage();
};

var setup_connection_listeners = function(connection){
	connection.on("error", on_connection_error);
	connection.on("data", on_connection_data);
	connection.on("close", on_connection_closed);

};

/* 
 *	Called after all initial connections are established. Finalizes initialization.
 * 	Will only work if both connect() and init_second_stage() finished.
 */
var init_last_stage = function init_last_stage(){
	if(all_connected && received_all_connections){
		log("Initialization complete, waiting for peers to start game...");
		initialization_ready_to_complete = true;
	}
};

/*	
 *	Parameters: 	len - desired length of random number array.
 *	Returns:		a single array of random numbers.
 */
var generate_random_array = function generate_random_array(len){
	result = Array(len);
	for(var i = 0; i < result.length; i++){result[i] = Math.random();}
	return result;
};

/*
 * Called when all data for the current frame is received by this peer.
 * this function decides what to do next
 */
var all_data_received = function(){
	if(first_stage_of_frame_submission_done){
		submit_frame_finalize();// finish processing for this frame
	}else{
		ready_to_finalize_frame_submission = true;
	}
};

/* 
 *	Called after all the data for current frame is received
 */
var submit_frame_finalize = function submit_frame_finalize(){
	var states = [];
	var merged_input = {};
	var random = {};
	
	for(var peer in frame_submissions){
		var frame = frame_submissions[peer];
		states.push(frame.state);
		merged_input[peer] = frame.user_input;
		random[peer] = frame.random_submission;
	}
	
	var popular_state = JSON.parse(popularity_poll(states));
	
	//Assign new random_numbers after reset_variables wipes the old ones, not before.
	random_numbers = combine_random_arrays(random); 

	// MUST be calculated before reset_variables call:
	var previous_frame_length = new Date().getTime() - timestamp_begin;
	
	reset_variables(); // reset all the tracking variables for this frame, like submission_count.
	current_frame++;
	//calls the dev-defined update function specified in the init.
	if(previous_frame_length >= min_frame_length_ms){
		// if minimum frame length already passed, call update function
		game_update_function(popular_state, merged_input);
	}else{
		// call update function when the minimum frame length was reached
		setTimeout(
			function(){game_update_function(popular_state, merged_input);},
			min_frame_length_ms - previous_frame_length
		);
	}
};

/*
 * Popularity poll function. Determines the state on which most of the peer agree
 * Input:
 * game_states(array of string) -- array of game states that compete in popularity poll
 * peer_ids -- array of peer ids
 *
 * returns the most popular state (still in serialized form)
 * 
 */
var popularity_poll = function popularity_poll(game_states){

	var gameStates = game_states;

	//2-D array to keep old scores in. 
	var popScore = Array(gameStates.length);
	for(var i = 0; i < gameStates.length - 1; i++){
		popScore[i] = Array(gameStates.length);
	}
	for(var i = 0; i < gameStates.length - 1; i++){
		//It's a doubly nested for loop, but redundant calculations are cut out.
		for(var j = i + 1; j < gameStates.length; j++){
			
			if(typeof gameStates[i] == "string"){
				
				if(typeof gameStates[j] == "string"){
					if(gameStates[i] == gameStates[j]){
						// replace the string with the index of its match. 
						// If other  things want to calculate with him. 
						gameStates[j] = i;
						popScore[i][j] = 1;	//perfect match is worth 100 points.
					}
					else{
						var points = gameStates[i].score(gameStates[j], 1);
						//Assumes that i.score(j) == j.score(i)
						popScore[i][j] = points;
					}
				}
				
				//If it's not a string, then the index referenced has a perfect match.
				else{

				//copy data from perfect match instead of recalculating score.
				popScore[i][j] = popScore[gameStates[j]][i]; 
				}
			}

			//Same idea as before, but the perfect-matched index is gameStates[i] instead of j
			else{
				// copies previously calculated score into the specified spot.
				popScore[i][j] = popScore[gameStates[i]][j];
			}
		}
	}
	
	var bestStateIndex = 0; 	//Now calculate total scores and find best index.
	var maxValue = 0;
	for (var i = 0; i < gameStates.length; i++){
		if(typeof gameStates[i] == "string"){	//skip over indexes that were an exact match.
			var sum = 0;
			var k = 0;
			var l = i;
			
			for(var j = 0; j < gameStates.length - 1; j++){
				if(k == i){
					l++;
					sum += popScore[k][l]
				}
				else if(k < i){
					sum += popScore[k][l]
					k++
				}
			}
			if(sum > maxValue){
				maxValue = sum;
				bestStateIndex = i;
			}
		}
	}
	
	return gameStates[bestStateIndex];
};

/*	
 *	Parameters: 	frame_random - object containing key->value pairs of peerID->random_array
 *					peer_priority_list - array of peer IDs sorted in priority order.
 *	Returns:		a single array of random numbers.
 */
var combine_random_arrays = function combine_random_arrays(frame_random){
	var peer_priority_list = peer_order;
	return [].concat.apply([], 
		peer_priority_list.map(function(peer_ID){return frame_random[peer_ID];}))
};

/*
 * Reset variables involved in tracking the state of the current frame.
 * Also moves future variables to the present.
 */
var reset_variables = function reset_variables(){
	submission_count = future_submission_count;
	frame_submissions = future_frame_submissions;
	submission_check = future_submission_check;
	
	future_submission_count = 0;
	future_frame_submissions = {};
	future_submission_check = {};

	fixing_packet_loss = false;
	
	first_stage_of_frame_submission_done = false;
	ready_to_finalize_frame_submission = false;

	nth_num = 0;

	// for timing the game update function together with next
	// iteration of the frame submission. Used to limit FPS
	timestamp_begin = new Date().getTime();
};


// PEER EVENTS

// called when connection with server is closed:
var on_peer_destroyed = function(peer_id){
	log("Destroyed peer w/ id: ", peer_id);
};

var on_new_connection_to_this_peer = function(dataConnection){
	connections_in_progress[dataConnection.peer];
	log("Peer w/ id:", dataConnection.peer, "established new connection to me");
	dataConnection.on("open", on_connection_successfully_established);
};

/*
 * Called when peer disconnects from server
 * Reconnects right back and makes sure to connect to all peers that it does not have
 * a connection to.
 * peer -- peer object which got disconnected from the server
*/
var on_peer_server_disconnection = function(peer){
	err("Disconnected from server, handling not currently supported");
};

var on_peer_error = function(error){
	log("Peer error: ", error);
};

// END of PEER EVENTS

// CONNECTION EVENTS:

// when connection was sucessfully established
// note that peer_id is not natively passed during this event (I added work-around)
var on_connection_successfully_established = function(){
	
	// this should be called regardless of whether connection is already in connections or not
	setup_connection_listeners(this); // setup listeners for this connection
	delete connections_in_progress[peer_id]; // no longer in progress

	var peer_id = this.peer;
	log("New connection with peer: ", peer_id);
	
	if(!connections[peer_id]){
		if(initialized && peer_order.indexOf(peer_id) == -1){
			war("WARNING: new connection to unrecognized peer after intialization stage.",
					"peer_order array will be incorrect. Peer id is:", peer_id);
		}

		// setup the rest of event listeners (on open is already setup and fired):

		connections[peer_id] = this; // save connection under peer id

		delete initial_connections_check[peer_id]; // this connection is good to go
		if(initial_connections_check.started == true){
			if(initialization_in_progress && Object.keys(initial_connections_check).length == 1){
				//If in initialization stage and no more connections left to establish, finish init
				received_all_connections = true;
				init_last_stage();
			}
		}
	}else{
		log("Warning: connection for this peer already exists, ignoring (listeners were still setup)")
	}

};
// on some (peer) connection closed to this peer
// note that peer_id is not natively passed during this event (I added work-around)
var on_connection_closed = function(){
	var peer_id = this.peer;
	//var id = 'unknown';
	log("Connection closed with peer: ", peer_id)
};
// on new data arriving:
var on_connection_data = function(data){
	var id = this.peer;
	log("New data arrived from id: ", id, data);

	var ignore = false;
	
	

	if(! connections[id]){
		war("WARNING: data submission from the unregistered peer", id);
		ignore = true;
	}
	//the data was from a valid peer.
	if(! ignore){
		//might be more efficient to split up the frame object now.
		var candidate_submission = JSON.parse(data);
		
		//Log warning when receiving outdated frames.
		if(candidate_submission.frame < current_frame){
			war("WARNING: received outdated frame data from peer", id);
		}
		//data received is from a future frame. This peer is behind.
		else if(candidate_submission.frame > current_frame){
			if(fixing_packet_loss == false){
				//need to set fixing flag asap, use secondary flag to trigger...
				//response to packet loss after adding future frame to the proper objects.
				fixing_packet_loss = true;
				var start_fix = true;
			}
			if(future_submission_check[id]){
				war("WARNING: duplicate data submission for the peer ", id, "for the next frame.",
					"Initial submission: ", future_frame_submissions[id],"\nThis submission:", data);
			}
			else{
				future_frame_submissions[id] = candidate_submission;
				future_submission_check[id] = true;
			}
			
			//check if this peer has kicked into packet-loss-fixing mode. 
			if(start_fix){
				war("WARNING: received data from peer for a future frame, assumed due to packet loss.",
				"Completing submit_frame() immediately to catch up.")
				all_data_received();
			}
		//Not only has every peer sent data for this frame,
		//That other peer has had time to calculate state and submit again.
		//Probably means packet loss has occurred. 
		
		//Call all_data received, first adding empty frame data structures as necessary.
		//
		}
		
		//The peer received ordinary data as expected.
		else if(candidate_submission.frame == current_frame){
			//Block current frame packets while fixing packet loss.
			//This peer will be temporarily incorrect, but they'll synch up soon.
			if(fixing_packet_loss){
				war("WARNING: This peer is currently dealing with packet loss.",
				"Data submission from peer ", id, "for the current frame blocked.");
			}
			
			
			//check for duplicate submissions for this frame.
			else if(submission_check[id]){
				war("WARNING: duplicate data submission for the peer ", id, "for this frame.",
						"Initial submission: ", frame_submissions[id],"\nThis submission:", data);
			}
			else{
				frame_submissions[id] = candidate_submission;
				submission_check[id] = true;

				// note that for the following check, checking that lengths are equal is enough,
				// since submissions for wrong peers are filtered above
				if(Object.keys(submission_check).length == Object.keys(connections).length && 
						initialized == true){
					// data for all peers is submitted for this frame, can proceed
					log("Data for all peers submitted for this frame");
					all_data_received();
				}
			}
		}
	}else{
		war("WARNING: data submission was ignored for the peer", id, "due to errors above")
	}

};

// on some connection error
var on_connection_error = function(error){
	log("Error occured during connection: ", error)
};
// END of CONNECTION EVENTS

// END PRIVATE
/*--------------------------------------------------------------------------------*/
// PUBLIC:
var init, start_game, submit_frame, random, get_info, get_peers;

/*
 * Initialize state of the library, possibly setting some of them from arguments
 * game_update -- function that needs to be called for every frame, (main function of the game?)
 * this function will first be called when the initialization stage is complete, and then each
 * time after the submit_frame function is done with it's processing
 * options_object -- key:value pairs where key is for option name
 * available options:
 * server_info -- host, port, and path necessary for connecting to server
 * 		{host: "my.host.com", port: 123456, path: "/my/path"}
 * log_level -- log level, can be 0, 1, 2 for INFO, WARNING, ERROR respectively
 * any messages for level lower than specified are ignored. default: 1 (WARNING)
 */
init = function init(game_update, options_object){
	if(options_object.log_level != null){
		LOG_LEVEL = options_object.log_level;
	}

	if(initialized){
		log("Already initialized!");
		return;
	}else if(initialization_in_progress){
		log("Initialization is already in progress");
		return;
	}
	initialization_in_progress = true;

	// check that game_update is supplied
	if(game_update == null || (typeof game_update) != 'function'){
		log("You must supply game_update function as first parameter to the",
				"init function of nlib. This funcion will be called each frame and should",
				"contain your game's frame simulation code. It should take state and inputs",
				"objects as parameters")
	}else{
		game_update_function = game_update;
	}

	options_object = options_object || {};
	options = options_object; // set global options TODO: more defaults?

	var server_info = options_object.server_info || { // defaults:
		host: 'nlib-server.herokuapp.com',
		port: 80,
		path: '/peerjs_server'
	};
	options.server_info = server_info;

	my_peer = new Peer({host: server_info.host, port: server_info.port,
		path: server_info.path}); // init own peer

	// NEXT SECTION SHOULD GO LAST,
	// EVERYTHING THAT NEEDS TO GO AFTER IT, SHOULD GO INTO init_second_stage
	// note: rest of the events are setup when peer is sucessfully created
	my_peer.on("open", function(id){init_second_stage(id)});
	my_peer.on("error", on_peer_error);

};

/*
 * Call on every peer after finishing initialization to actually begin the game.
 */
start_game = function start_game(){
	if(initialization_ready_to_complete == true){
		initialized = true;
		initialization_in_progress = false;
		log("Started game!")
		submit_frame({}, {}); // the first frame submission to redistribute initial random numbers
	}
	else{
		war("WARNING: Attempted to start game before initialization was complete.")
	}
}
	
//Public function to get a reference to sorted list of peers.
get_peers = function get_peers(){
	return peer_order.slice(0)
}

/*
 * Submit changed frame after performing game step and modifying data
 * state(object) -- state of the game
 * user_input(any serializable) -- input collected from the user for the next frame
 * 		explanation: because user input needs to be redistributed between peers before it is
 * 		processed, it will need to be late by one frame each time. You are advised to collect
 * 		user input (key presses etc.) at the end of the frame simulation.
 */
submit_frame = function submit_frame(state, user_input){
	//parameter to generate_random_array could be changed to be developer defined.
	var rand = generate_random_array(10); 
	var str_state = stable_stringify(state);
	var my_submission = DataFrame(str_state, user_input, rand);
	var frame = JSON.stringify(my_submission);

	// add my own submission to the frame submissions
	frame_submissions[my_peer.id] = my_submission;
	
	log("Submitting frame");
	
	//Send the frame object:
	for(var peer_id in connections){
		connections[peer_id].send(frame);
	}

	// NOTE: the second stage of this function, submit_frame_finalize is called when data from
	// all the other peers is received
	if(ready_to_finalize_frame_submission){
		submit_frame_finalize();
	}else{
		first_stage_of_frame_submission_done = true;
		if(Object.keys(submission_check).length == Object.keys(connections).length && initialized == true){
			// data for all peers is submitted for this frame, can proceed
			log("Data for all peers submitted for this frame");
			all_data_received();
		}
	}	
};

/*
 * Returns random number that is synchronized between peers (guaranteed to be same for the
 * same call number on all peers)
 */
random = function random(){
	var result = random_numbers[nth_num % random_numbers.length];
	nth_num++;
	return result;
}

get_info = function get_info(){
	return {
		my_peer: my_peer,
		peers_connected: Object.keys(connections),
		peer_order: peer_order.slice(0), // easiest way to make shallow copy
		random_numbers: random_numbers
		
	}
};

// END PUBLIC
/*--------------------------------------------------------------------------------*/

return {
	init: init,
	submit_frame: submit_frame,
	get_info: get_info,
	random: random,
	get_peers: get_peers,
	start_game: start_game
}
/*--------------------------------------------------------------------------------*/
// END closure
})();
